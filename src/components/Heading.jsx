import React from 'react';
import PropTypes from 'prop-types';

function Heading({ children, h }) {
  const classList = ['mb-5'];

  if (h === '1') {
    return <h1 className={classList.join(' ')}>{children}</h1>;
  } if (h === '2') {
    return <h2 className={classList.join(' ')}>{children}</h2>;
  } if (h === '3') {
    return <h3 className={classList.join(' ')}>{children}</h3>;
  } if (h === '4') {
    return <h4 className={classList.join(' ')}>{children}</h4>;
  } if (h === '5') {
    return <h5 className={classList.join(' ')}>{children}</h5>;
  } if (h === '6') {
    return <h6 className={classList.join(' ')}>{children}</h6>;
  }

  return (
    <h1 className={classList.join(' ')}>{children}</h1>
  );
}

Heading.propTypes = {
  children: PropTypes.node,
  h: PropTypes.string,
};

Heading.defaultProps = {
  children: null,
  h: '1',
};

export default Heading;
