import axios from 'axios';

const axiosSuperheroAPIInstance = axios.create({
  baseURL: 'https://jsonplaceholder.typicode.com/',
});

export default axiosSuperheroAPIInstance;
