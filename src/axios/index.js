import axiosAlkemy from './axiosAlkemyAPI';
import axiosJsonPlaceHolder from './axiosJsonPlaceHolderAPI';

export {
  axiosAlkemy,
  axiosJsonPlaceHolder,
};
