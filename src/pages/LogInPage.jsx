import React from 'react';
import { Container, Row } from 'react-bootstrap';
import Heading from '../components/Heading';
import LogInForm from '../components/LogInForm';

function LogInPage() {
  return (
    <Container>
      <Heading>Log In</Heading>
      <Row className="justify-content-center">
        <LogInForm />
      </Row>
    </Container>
  );
}

export default LogInPage;
