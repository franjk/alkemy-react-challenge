import React, { useEffect, useState } from 'react';
import { Button, Container, Spinner } from 'react-bootstrap';
import { Link, Redirect, useHistory } from 'react-router-dom';
import { useSelector } from 'react-redux';
import { axiosJsonPlaceHolder } from '../axios';
import Post from '../components/Post';
import Alert from '../components/Alert';
import Heading from '../components/Heading';

function HomePage() {
  const history = useHistory();
  const auth = useSelector((state) => state.auth);
  const [posts, setPosts] = useState([]);
  const [error, setError] = useState(null);
  const [alert, setAlert] = useState();

  if (!auth.token) {
    return <Redirect to="/login" />;
  }

  useEffect(async () => {
    try {
      const getPosts = await axiosJsonPlaceHolder.get('posts');
      setPosts(getPosts.data);
    } catch (err) {
      setError('No se pudieron recuperar los posts');
    }
  }, []);

  if (!alert && history.location.state?.message) {
    setAlert(history.location.state.message);
  }

  const deletePost = async (postId) => {
    try {
      const res = await axiosJsonPlaceHolder.delete(`posts/${postId}`);
      if (res) {
        setAlert(`Post ${postId} borrado exitosamente`);
        setPosts(posts.filter((post) => post.id !== postId));
      }
    } catch (err) {
      setError('Error borrando el post');
    }
  };

  return (
    <Container>
      {error && <Alert message={error} variant="danger" />}
      {alert && <Alert message={alert} /> }

      <Heading>Home</Heading>

      <div className="mb-4">
        <Button as={Link} to="/posts/create">Crear nuevo post</Button>
      </div>
      <div>
        {posts ? (
          posts.map((post) => (
            <div key={post.id} className="mb-4">
              <Post
                id={post.id}
                title={post.title}
                actions={{
                  deletion: () => deletePost(post.id),
                  edit: `/posts/${post.id}/edit`,
                  details: `/posts/${post.id}`,
                }}
              />
            </div>
          ))
        ) : (
          <Spinner />
        )}
      </div>
    </Container>
  );
}

export default HomePage;
