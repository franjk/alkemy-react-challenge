import React from 'react';
import { Button, Form } from 'react-bootstrap';
import { useFormik } from 'formik';
import { useHistory } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import * as Yup from 'yup';
import { axiosAlkemy } from '../axios';
import { storeToken } from '../auth/authSlice';

function LogInForm() {
  const history = useHistory();
  const dispatch = useDispatch();

  const formik = useFormik({
    initialValues: {
      email: '',
      password: '',
    },
    validationSchema: Yup.object({
      email: Yup.string()
        .email('Dirección de email inválida')
        .required('Requerido'),
      password: Yup.string()
        .max(15, 'Deben ser 15 caracteres o menos')
        .required('Requerido'),
    }),
    onSubmit: async (values) => {
      const { email, password } = values;

      let res;
      try {
        res = await axiosAlkemy.post('', { email, password });
        if (res.status === 200) {
          dispatch(storeToken(res.data.token));
          history.push('/');
        } else {
          throw new Error(res.data);
        }
      } catch (err) {
        formik.errors.email = 'Email invalido. Intenta \'challenge@alkemy.org\'';
        formik.errors.password = 'Password invalido. Intenta \'react\'';
      }
    },
  });

  return (
    <div>
      <Form noValidate onSubmit={formik.handleSubmit}>
        <Form.Group className="mb-3" controlId="email">
          <Form.Label>Direccion de Email</Form.Label>
          <Form.Control
            type="email"
            placeholder="Email"
            name="email"
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            value={formik.values.email}
            isInvalid={!!formik.errors.email}
          />
          <Form.Control.Feedback type="invalid">
            {formik.errors.email}
          </Form.Control.Feedback>
        </Form.Group>

        <Form.Group className="mb-3" controlId="password">
          <Form.Label>Password</Form.Label>
          <Form.Control
            type="password"
            placeholder="Password"
            name="password"
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            value={formik.values.password}
            isInvalid={!!formik.errors.password}
          />
          <Form.Control.Feedback type="invalid">
            {formik.errors.password}
          </Form.Control.Feedback>
        </Form.Group>
        <Button variant="primary" type="submit">
          Enviar
        </Button>
      </Form>
    </div>
  );
}

export default LogInForm;
