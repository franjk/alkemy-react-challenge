import React, { useEffect, useState } from 'react';
import { Container, Spinner } from 'react-bootstrap';
import { useParams, Redirect } from 'react-router-dom';
import { useSelector } from 'react-redux';
import { axiosJsonPlaceHolder } from '../axios';
import PostForm from '../components/PostForm';
import Alert from '../components/Alert';
import Heading from '../components/Heading';

function EditPostPage() {
  const { postId } = useParams();
  const auth = useSelector((state) => state.auth);
  const [post, setPost] = useState();
  const [error, setError] = useState();

  if (!auth.token) {
    return <Redirect to="/login" />;
  }

  useEffect(async () => {
    try {
      const res = await axiosJsonPlaceHolder.get(`posts/${postId}`);
      setPost(res.data);
    } catch (err) {
      setError('No se pudo recuperar el post');
    }
  }, []);

  return (
    <Container>
      {error && <Alert message={error} variant="danger" />}

      <Heading>Editar Post</Heading>

      <div className="mb-3">
        {post ? (
          <PostForm post={post} />
        ) : (
          <Spinner />
        )}
      </div>

    </Container>
  );
}

export default EditPostPage;
