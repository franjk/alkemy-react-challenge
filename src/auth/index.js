export const storeToken = (token) => {
  window.localStorage.setItem('token', token);
  return true;
};

export const retrieveToken = () => window.localStorage.getItem('token');
