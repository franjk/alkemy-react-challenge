import React from 'react';
import PropTypes from 'prop-types';
import { Button, Card } from 'react-bootstrap';
import { Link } from 'react-router-dom';

function Post({
  title, body, actions,
}) {
  const { deletion, edit, details } = actions;

  return (
    <Card>
      <Card.Body>
        <Card.Title>{title}</Card.Title>

        {body && (
          <Card.Text>
            {body}
          </Card.Text>
        )}

        {deletion && <Button className="text-danger" variant="link" onClick={deletion}>Eliminar</Button>}
        {edit && <Button variant="link" as={Link} to={edit}>Editar</Button>}
        {details && <Button variant="link" as={Link} to={details}>Detalle</Button>}

      </Card.Body>
    </Card>
  );
}

Post.propTypes = {
  title: PropTypes.string.isRequired,
  body: PropTypes.string,
  actions: PropTypes.shape({
    deletion: PropTypes.func,
    edit: PropTypes.string,
    details: PropTypes.string,
  }),
};

Post.defaultProps = {
  actions: {
    deletion: null,
    edit: null,
    details: null,
  },
  body: '',
};

export default Post;
