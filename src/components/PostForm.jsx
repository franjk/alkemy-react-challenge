import React from 'react';
import PropTypes from 'prop-types';
import { Button, Form } from 'react-bootstrap';
import { useFormik } from 'formik';
import { useHistory } from 'react-router-dom';
import * as Yup from 'yup';
import { axiosJsonPlaceHolder } from '../axios';

function PostForm({ post }) {
  const history = useHistory();
  const mode = post ? 'edit' : 'create';

  const formikInitialValues = {
    title: '',
    body: '',
  };

  if (mode === 'edit') {
    formikInitialValues.title = post.title;
    formikInitialValues.body = post.body;
  }

  const formik = useFormik({
    initialValues: {
      title: formikInitialValues.title,
      body: formikInitialValues.body,
    },
    validationSchema: Yup.object({
      title: Yup.string()
        .max(80, 'Debe tener como máximo 80 caracteres')
        .required('Requerido'),
      body: Yup.string()
        .max(1200, 'Debe tener como máximo 1200 caracteres')
        .required('Requerido'),
    }),
    onSubmit: async (values) => {
      const { title, body } = values;
      const userId = 1;

      let res;
      try {
        if (mode === 'create') {
          res = await axiosJsonPlaceHolder.post('posts', { title, body, userId });
          if (res) {
            const historyState = { message: `Post creado con la id: ${res.data.id}` };
            history.push('/', historyState);
          }
        } else if (mode === 'edit') {
          res = await axiosJsonPlaceHolder.put(`posts/${post.id}`, {
            title, body, userId,
          });
          if (res) {
            const historyState = { message: `Post ${post.id} editado` };
            history.push(`/posts/${post.id}`, historyState);
          }
        }
      } catch (err) {
        formik.errors.title = 'Título inválido';
        formik.errors.body = 'Contenido inválido';
      }
    },
  });

  return (
    <div>
      <Form noValidate onSubmit={formik.handleSubmit}>
        <Form.Group className="mb-3" controlId="title">
          <Form.Label>Titulo</Form.Label>
          <Form.Control
            type="text"
            placeholder="Ingresa el título del post"
            name="title"
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            value={formik.values.title}
            isInvalid={!!formik.errors.title}
          />
          <Form.Control.Feedback type="invalid">
            {formik.errors.title}
          </Form.Control.Feedback>
        </Form.Group>

        <Form.Group className="mb-3" controlId="body">
          <Form.Label>Contenido</Form.Label>
          <Form.Control
            as="textarea"
            placeholder="Ingresa el contenido del post"
            name="body"
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            value={formik.values.body}
            isInvalid={!!formik.errors.body}
            style={{ height: '200px' }}
          />
          <Form.Control.Feedback type="invalid">
            {formik.errors.body}
          </Form.Control.Feedback>
        </Form.Group>

        <Button variant="primary" type="submit">
          Guardar
        </Button>
      </Form>
    </div>
  );
}

PostForm.propTypes = {
  post: PropTypes.shape({
    title: PropTypes.string,
    body: PropTypes.string,
    id: PropTypes.number,
  }),
};

PostForm.defaultProps = {
  post: null,
};

export default PostForm;
