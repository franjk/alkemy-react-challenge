import React from 'react';
import { Container } from 'react-bootstrap';
import { Redirect } from 'react-router-dom';
import { useSelector } from 'react-redux';
import PostForm from '../components/PostForm';
import Heading from '../components/Heading';

function CreatePostPage() {
  const auth = useSelector((state) => state.auth);

  if (!auth.token) {
    return <Redirect to="/login" />;
  }

  return (
    <Container>

      <Heading h="2">Crear Post</Heading>

      <div className="mb-3">
        <PostForm />
      </div>

    </Container>
  );
}

export default CreatePostPage;
