import { createSlice } from '@reduxjs/toolkit';

export const counterSlice = createSlice({
  name: 'auth',
  initialState: {
    token: '',
  },
  reducers: {
    storeToken: (state, action) => {
      window.localStorage.setItem('token', action.payload);
      state.token = action.payload;
    },
    logOut: (state) => {
      window.localStorage.removeItem('token');
      state.token = '';
    },
  },
});

// Action creators are generated for each case reducer function
export const { storeToken, logOut } = counterSlice.actions;

export default counterSlice.reducer;
