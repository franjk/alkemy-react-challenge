import React from 'react';
import PropTypes from 'prop-types';
import { Alert } from 'react-bootstrap';

function MessageAlert({ message, variant }) {
  return (
    <Alert variant={variant}>

      {message}

    </Alert>

  );
}

MessageAlert.propTypes = {
  message: PropTypes.string.isRequired,
  variant: PropTypes.string,
};

MessageAlert.defaultProps = {
  variant: 'primary',
};

export default MessageAlert;
