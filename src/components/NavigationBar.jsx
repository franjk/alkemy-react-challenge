import React from 'react';
import {
  Navbar, Container, Nav,
} from 'react-bootstrap';
import { Link, useHistory } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { logOut } from '../auth/authSlice';

function NavigationBar() {
  const history = useHistory();
  const dispath = useDispatch();
  const auth = useSelector((state) => state.auth);

  const logout = () => {
    dispath(logOut());
    history.push('/login');
  };

  return (
    <Navbar bg="light" expand="lg">
      <Container>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="me-auto">
            <Nav.Link as={Link} to="/">Home</Nav.Link>
            <Nav.Link as={Link} to="/posts/create">Crear</Nav.Link>
          </Nav>
          <Nav>
            {auth.token ? (
              <Nav.Link onClick={logout}>Logout</Nav.Link>
            ) : (
              <Nav.Link as={Link} to="/login">Login</Nav.Link>
            )}
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}

export default NavigationBar;
