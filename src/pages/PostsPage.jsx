import React, { useEffect, useState } from 'react';
import { Button, Container, Spinner } from 'react-bootstrap';
import {
  Link, useHistory, useParams, Redirect,
} from 'react-router-dom';
import { useSelector } from 'react-redux';
import { axiosJsonPlaceHolder } from '../axios';
import Alert from '../components/Alert';
import Heading from '../components/Heading';

function PostsPage() {
  const history = useHistory();
  const { postId } = useParams();
  const auth = useSelector((state) => state.auth);
  const [post, setPost] = useState();
  const [error, setError] = useState();
  const [alert, setAlert] = useState();

  if (!auth.token) {
    return <Redirect to="/login" />;
  }

  useEffect(async () => {
    try {
      const res = await axiosJsonPlaceHolder.get(`posts/${postId}`);
      setPost(res.data);
    } catch (err) {
      setError('No se pudo recuperar el post');
    }
  }, []);

  if (!alert && history.location.state?.message) {
    setAlert(history.location.state.message);
  }

  const deletePost = async () => {
    try {
      const res = await axiosJsonPlaceHolder.delete(`posts/${postId}`);
      if (res) {
        history.push('/', { message: `Post ${postId} borrado exitosamente` });
      }
    } catch (err) {
      setError('Error borrando el post');
    }
  };

  return (
    <Container>
      {error && <Alert message={error} variant="danger" />}
      {alert && <Alert message={alert} /> }

      {post ? (
        <>
          <Heading>{post.title}</Heading>

          <div className="mb-3">
            {post.body}
          </div>

          <div className="mb-3">
            <Button className="text-danger" variant="link" onClick={() => deletePost()}>Eliminar</Button>
            <Button variant="link" as={Link} to={`/posts/${postId}/edit`}>Editar</Button>
          </div>

        </>
      ) : (
        <Spinner />
      )}
    </Container>
  );
}

export default PostsPage;
