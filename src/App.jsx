import React from 'react';
import { Switch, Route } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { storeToken } from './auth/authSlice';
import { retrieveToken } from './auth';
import HomePage from './pages/HomePage';
import LogInPage from './pages/LogInPage';
import PostsPage from './pages/PostsPage';
import CreatePostPage from './pages/CreatePostPage';
import EditPostPage from './pages/EditPostPage';
import NavigationBar from './components/NavigationBar';

function App() {
  const token = retrieveToken();
  const dispatch = useDispatch();

  if (token) {
    dispatch(storeToken(token));
  }

  return (
    <div>
      <header className="mb-5">
        <NavigationBar />
      </header>
      <main>
        <Switch>
          <Route path="/login" component={LogInPage} />
          <Route path="/posts" component={HomePage} exact />
          <Route path="/posts/create" component={CreatePostPage} />
          <Route path="/posts/:postId/edit" component={EditPostPage} />
          <Route path="/posts/:postId" component={PostsPage} />
          <Route path="/" component={HomePage} />
        </Switch>

      </main>
    </div>
  );
}

export default App;
